# Timekeeper

This little application keeps track of your working items and is able to log it to your jira tickets.

![Example](docs/Example.PNG)

## How to install?

You need a working python (>=3.11) installation. Then you can run:

```
$ pip install git+https://gitlab.com/florianrenneke/timekeeper.git
$ timer
```

It saves the time reports in a sqlite database. You can use it for post processing it later on.