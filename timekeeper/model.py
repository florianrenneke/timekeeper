from abc import ABC
from datetime import datetime
import math
import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QLabel, QHBoxLayout, QWidget
from PyQt5.QtCore import QTimer, QTime
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCursor
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QLabel, QVBoxLayout, QWidget, QMenu, QAction, QLineEdit
from PyQt5.QtCore import QTimer, QTime, Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import pyqtSignal
import re
from .jira_logger import log_work_time_to_jira, check_if_ticket_exists
from PyQt5.QtGui import QPalette, QColor
from datetime import datetime
import os
from sqlalchemy import create_engine, Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QLabel, QVBoxLayout, QWidget
from PyQt5.QtCore import QTimer, QTime

database_url = 'sqlite:///timeTable.sqlite'
engine = create_engine(database_url)
Session = sessionmaker(bind=engine)

Base = declarative_base()

class TimeTable(Base):
    __tablename__ = 'timeTable'

    id = Column(Integer, primary_key=True)
    key = Column(String)
    project = Column(String)
    elapsed_seconds = Column(Integer)
    end_time = Column(DateTime)

    @classmethod
    def insert_entry(cls, key, project, elapsed_seconds):
        """Insert a new entry into the TimeTable."""
        new_entry = cls(key=key, project=project, elapsed_seconds=elapsed_seconds, end_time=datetime.now())

        session = Session()

        session.add(new_entry)
        session.commit()

        session.close()

    @classmethod
    def get_all_keys(cls):
        session = Session()
        keys = session.query(cls.key).all()
        session.close()
        return keys


def get_jira_project(input_string):
    match_str = re.findall(r'^([A-Z]+)-\d+$', input_string)
    if not match_str:
        return []

    if check_if_ticket_exists(input_string):
        return match_str
    return []

class TimeTableModel:

    def __init__(self) -> None:
        Base.metadata.create_all(engine)

    def load_items(self) -> list[str]:
        self.items = {"Pause": 0}
        self.items.update({key[0]: 0 for key in TimeTable.get_all_keys()})

        self.current_item = None
        return self.items.keys()

    def overwrite_elapsed_time(self, text:str):
        time = QTime.fromString(text, "hh:mm:ss")
        if time.isValid():
            seconds = time.second() + time.minute() * 60 + time.hour() * 3600

            self.items[self.current_item] = seconds
            print("OVERWRITING", text)
            print(seconds)

    def start_new(self, text:str):

        if self.current_item is not None:
            print(f"Stopping {self.current_item}")

            seconds = self.items[self.current_item]

            if seconds<60:
                print("You must work at least 1 minute on a task to be logged")
            else:
                proj = get_jira_project(self.current_item)
                if proj:
                    print(f"Logging {round(seconds/60)} min to Jira")
                    log_work_time_to_jira(self.current_item, f"{round(seconds/60)}m", "Automated logging tool timekeeper: https://github.com/Renneke/timekeeper")

                    # time reset to 0 when logged
                    TimeTable.insert_entry(self.current_item, proj[0], self.items[self.current_item])
                    self.items[self.current_item] = 0
                else:
                    # no jira project TODO: Add entry to sqlite
                    # time reset to 0 when logged
                    TimeTable.insert_entry(self.current_item, self.current_item, self.items[self.current_item])
                    self.items[self.current_item] = 0

        print(f"Starting new event: {text}")
        self.current_item = text
        if self.current_item not in self.items:
            self.items[self.current_item] = 0

    def get_elapsed_seconds(self):
        if self.current_item is None:
            return 0
        return self.items[self.current_item]

    def add_second(self):
        if self.current_item is None:
            return
        self.items[self.current_item] += 1
