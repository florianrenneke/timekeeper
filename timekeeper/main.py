from datetime import datetime
import os
from sqlalchemy import create_engine, Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QLabel, QVBoxLayout, QWidget
from PyQt5.QtCore import QTimer, QTime
from .combobox import MyWindow


def main():
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    app.exec_()


