import sys
import json
import warnings
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QFileDialog, QDialog, QDialogButtonBox

from jira import JIRA

CONFIG_FILE_PATH = './config.json'

def load_credentials_from_config(config_file):
    try:
        with open(config_file, 'r') as file:
            config = json.load(file)
            return config
    except FileNotFoundError:
        return None
    except json.JSONDecodeError:
        return None

def save_credentials_to_config(username, password, url):
    config = {
        "username": username,
        "password": password,
        "jira_url": url
    }
    with open(CONFIG_FILE_PATH, 'w') as file:
        json.dump(config, file)
    print(f"Configuration saved to '{CONFIG_FILE_PATH}'")


# Create a basic PyQt5 dialog to enter the username and password.
from PyQt5.QtWidgets import (
    QDialog, QVBoxLayout, QLabel, QLineEdit, QDialogButtonBox
)
from PyQt5.QtCore import Qt

class CredentialsDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()

        # Create labels and input fields
        self.url_label = QLabel("URL:")
        layout.addWidget(self.url_label)
        self.url_input = QLineEdit()
        layout.addWidget(self.url_input)

        self.username_label = QLabel("Username:")
        layout.addWidget(self.username_label)
        self.username_input = QLineEdit()
        layout.addWidget(self.username_input)

        self.password_label = QLabel("Password:")
        layout.addWidget(self.password_label)
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.Password)
        layout.addWidget(self.password_input)

        # Create a QDialogButtonBox with OK and Cancel buttons
        buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttons.accepted.connect(self.accept)  # Accept button
        buttons.rejected.connect(self.reject)  # Cancel button
        layout.addWidget(buttons)

        # Set alignment for labels and input fields
        self.url_label.setAlignment(Qt.AlignLeft)
        self.username_label.setAlignment(Qt.AlignLeft)
        self.password_label.setAlignment(Qt.AlignLeft)
        self.url_input.setAlignment(Qt.AlignLeft)
        self.username_input.setAlignment(Qt.AlignLeft)
        self.password_input.setAlignment(Qt.AlignLeft)

        # Add some spacing between elements
        layout.addSpacing(10)

        self.setLayout(layout)
        self.setWindowTitle("Jira Login")
        self.setGeometry(100, 100, 300, 200)  # Set the size of the dialog
        self.setStyleSheet("font-size: 12px;")  # Adjust font size
        self.show()

    def accept(self):
        username = self.username_input.text()
        password = self.password_input.text()
        url = self.url_input.text()
        if not username or not password or not url:
            return

        save_credentials_to_config(username, password, url)
        super().accept()


def check_if_ticket_exists(ticket_key):
    config = load_credentials_from_config(CONFIG_FILE_PATH)
    if config is None:
        print(f"Configuration file '{CONFIG_FILE_PATH}' not found or invalid.")
        credentials_dialog = CredentialsDialog()
        credentials_dialog.exec()
        config = load_credentials_from_config(CONFIG_FILE_PATH)
        if config is None:
            print("No time logged.")
            return False
    try:
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=Warning)
            jira = JIRA(server=config["jira_url"], basic_auth=(config["username"], config["password"]), options={"verify":False})
            jira.issue(ticket_key)
    except Exception:
        return False
    return True


def log_work_time_to_jira(ticket_key, time_spent, comment):

    config = load_credentials_from_config(CONFIG_FILE_PATH)
    if config is None:
        print(f"Configuration file '{CONFIG_FILE_PATH}' not found or invalid.")
        credentials_dialog = CredentialsDialog()
        credentials_dialog.exec()
        config = load_credentials_from_config(CONFIG_FILE_PATH)
        if config is None:
            print("No time logged.")
            return False

    try:
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=Warning)
            jira = JIRA(server=config["jira_url"], basic_auth=(config["username"], config["password"]), options={"verify":False})

            jira.issue(ticket_key)

        # Add worklog to the Jira ticket
        jira.add_worklog(ticket_key, timeSpent=time_spent, comment=comment)
        print(f"Worklog added to {ticket_key} successfully.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return False

    return True


if __name__ == '__main__':
    app = QApplication(sys.argv)


    log_work_time_to_jira('ANAGEN-123', '10m', 'Testing loggin feature')
