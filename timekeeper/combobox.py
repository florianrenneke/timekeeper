from abc import ABC
from datetime import datetime
import math
import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QLabel, QHBoxLayout, QWidget
from PyQt5.QtCore import QTimer, QTime
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCursor
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QLabel, QVBoxLayout, QWidget, QMenu, QAction, QLineEdit
from PyQt5.QtCore import QTimer, QTime, Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import pyqtSignal
import re
from .jira_logger import log_work_time_to_jira, check_if_ticket_exists
from PyQt5.QtGui import QPalette, QColor
from .model import TimeTableModel



class CustomComboBox(QComboBox):

    textChanged = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.setEditable(True)
        self.lineEdit().editingFinished.connect(self.textChangedCallback)
        self.currentIndexChanged.connect(self.textChangedCallback)
        self.setMinimumWidth(200)
        self.previously_selected_text = ""


        # Apply a color theme style to the QComboBox
        self.setStyleSheet("""
            QComboBox {
                background-color: #055e55; /* Background color */
                color: white; /* Text color */
                selection-background-color: #3498DB; /* Selected item background color */
                selection-color: white; /* Selected item text color */
            }
        """)

    def textChangedCallback(self):
        text = self.currentText()
        if not text:
            self.lineEdit().setText(text)
            return
        if text != self.previously_selected_text:
            self.previously_selected_text = text
            self.textChanged.emit(text)
            if self.findText(text) == -1:
                self.addItem(text)


    def focusInEvent(self, event):
        # Clear the text while keeping the items intact
        self.lineEdit().clear()
        super().focusInEvent(event)

    def focusOutEvent(self, event) -> None:
        self.lineEdit().setText(self.previously_selected_text)
        return super().focusOutEvent(event)

class CustomLineEdit(QLineEdit):
    focusOut = pyqtSignal()

    def focusOutEvent(self, event) -> None:
        self.focusOut.emit()
        return super().focusOutEvent(event)
class CustomLabel(QLabel):
    textChanged = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.my_parent = parent
        self.setFocusPolicy(Qt.FocusPolicy.StrongFocus)  # Allow the label to accept focus
        self.setStyleSheet("QLabel { color : white; font-weight: bold; }")

        self.setFrameShape(QLabel.Panel)  # Add a frame to indicate it's editable
        self.setAlignment(Qt.AlignCenter)

        # Create an editable line edit widget
        self.line_edit = CustomLineEdit(self)
        self.line_edit.hide()
        self.line_edit.returnPressed.connect(self.finish_editing)
        self.line_edit.focusOut.connect(self.finish_editing)

    def mouseDoubleClickEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.start_editing()

    def start_editing(self):
        self.line_edit.setText(self.text())
        self.line_edit.selectAll()
        self.line_edit.show()
        self.line_edit.setFocus(Qt.OtherFocusReason)

    def finish_editing(self):
        if self.line_edit.isVisible():
            new_text = self.line_edit.text()
            self.textChanged.emit(new_text)
            self.setText(new_text)
            self.line_edit.hide()
            self.setFocus()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.line_edit.hide()
        elif event.key() == Qt.Key_Return or event.key() == Qt.Key_Enter:
            self.finish_editing()
        else:
            super().keyPressEvent(event)

    def contextMenuEvent(self, event):
        context_menu = QMenu(self)
        exit_action = QAction("Exit", self)
        exit_action.triggered.connect(QApplication.quit)
        context_menu.addAction(exit_action)

        context_menu.exec_(event.globalPos())

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.my_parent.dragging = True
            self.my_parent.offset = event.globalPos() - self.my_parent.pos()
            event.accept()

    def mouseMoveEvent(self, event):
        if self.my_parent.dragging:
            self.my_parent.move(event.globalPos() - self.my_parent.offset)
            event.accept()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.my_parent.dragging = False



class MyWindow(QMainWindow):
    def __init__(self, model : TimeTableModel = TimeTableModel()):
        super().__init__()

        self.model = model
        self.initUI()

    def initUI(self):
        # Set window flags to make it frameless and always on top
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        palette = self.palette()
        palette.setColor(QPalette.Window, QColor(0, 122, 107))  # Change to your desired color
        self.setPalette(palette)
        self.setStyleSheet("QMainWindow { border: 2px solid #055e55; }")

        #self.setAttribute(Qt.WA_TranslucentBackground)
        self.comboBox = CustomComboBox()

        for item in self.model.load_items():
            self.comboBox.addItem(item)

        self.comboBox.setCurrentIndex(0)

        # Variables for moving the window
        self.dragging = False
        self.offset = None

        self.centralWidget = QWidget(self)
        self.setMaximumHeight(40)
        self.setCentralWidget(self.centralWidget)

        layout = QHBoxLayout(self.centralWidget)
        layout.addWidget(self.comboBox)

        self.elapsed_time_label = CustomLabel(self)
        layout.addWidget(self.elapsed_time_label)

        # Timer to update elapsed time
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_elapsed_time)
        self.timer.start(1000)  # Update every 1 second

        self.start_time = QTime.currentTime()
        self.elapsed_time_label.setText("Not running")

        self.elapsed_time_label.setFocus()

        self.comboBox.textChanged.connect(self.model.start_new)
        self.elapsed_time_label.textChanged.connect(self.model.overwrite_elapsed_time)


    def update_elapsed_time(self):

        self.model.add_second()

        formatted_time = QTime.fromMSecsSinceStartOfDay(self.model.get_elapsed_seconds()*1000).toString('hh:mm:ss')
        self.elapsed_time_label.setText(f"{formatted_time}")


    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.dragging = True
            self.offset = event.globalPos() - self.pos()
            event.accept()

    def mouseMoveEvent(self, event):
        if self.dragging:
            self.move(event.globalPos() - self.offset)
            event.accept()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.dragging = False

